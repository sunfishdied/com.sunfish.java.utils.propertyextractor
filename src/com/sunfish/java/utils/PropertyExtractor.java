package com.sunfish.java.utils;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;  
import java.util.zip.ZipException;  
import java.util.zip.ZipFile;


public class PropertyExtractor {

	public static String[] i18n = new String[]{
	"af","sq","ar","eu","bg","be","ca","hr","cs","da",
	"nl","en","eo","et","fo","fi","fr","gl","de","el",
	"iw","hu","is","ga","it","ja","ko","lv","lt","mk",
	"mt","no","pl","pt","ro","ru","gd","sr","sk","sl",
	"es","sv","tr","uk","zh","cs","tw","cn"
	};
	public static StringBuilder files = new StringBuilder();
	public static ArrayList<String> jarFiles= new ArrayList<String>();
	public static Hashtable<String,Properties> ps = new Hashtable<String,Properties>();
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		//判断输入是否正确
		if(args.length!=1 )
		{
			System.out.println("usage : java THISCLASS FOLDER_TO_EXTRACT");
			return;
		}
		Properties defaultLanguage = new Properties();
		String currentPath = System.getProperty("user.dir");
		ps.put("en", defaultLanguage);
		
		String iPath = args[0];
		
		System.out.println("searching "+iPath);
		
		getAllJar(iPath);
		
		
		

		for(Iterator j = jarFiles.iterator();j.hasNext();)
		{
			try
			{
				getProperties((String)j.next());
			}catch(Exception e)
			{
			System.out.println(e.getMessage());	
			}
		}
		
		String currentKey,currentLanguage;
		Properties en = ps.get("en");
		//生成排序后的key清单
		List<String> uskeys = new ArrayList<String>();
		for(Iterator ks = en.keySet().iterator();ks.hasNext();)
    	{
    		currentKey = (String)ks.next();
    		uskeys.add(currentKey);
    	}
		Collections.sort(uskeys);
		
		int merged=0;
		for (Iterator<String> it = ps.keySet().iterator(); it.hasNext(); ) 
		{
		
		    currentLanguage = (String) it.next();
	    	Properties v = ps.get(currentLanguage);
			System.out.println(currentLanguage + ":");
			merged=0;
			Properties px = new Properties();
		    if(!currentLanguage.equals("en")) 
		    {
		    
		    	for(Iterator ks = uskeys.iterator();ks.hasNext();)
		    	{
		    		currentKey = (String)ks.next();

		    		if(v.containsKey(currentKey)){
		    		px.put(currentKey,v.get(currentKey));
		    		}else{
		    		px.put(currentKey,en.get(currentKey));
		    		merged++;
		    		}
		    		
		    	}
		    	//写文件
		    	px.store(new FileOutputStream(currentPath + "//" + currentLanguage+".properties"), "AutoMerged Properties "+currentLanguage);
		    }else
		    {
		    	for(Iterator ks = uskeys.iterator();ks.hasNext();)
		    	{
		    		currentKey = (String)ks.next();
		    		px.put(currentKey, en.get(currentKey));
		    	}
		    	
		    	
		    	px.store(new FileOutputStream(currentPath + "//" + currentLanguage+".properties"), "AutoMerged Properties "+currentLanguage);
		    }
		    
			System.out.println(String.format("= %s merged and ordered, %s total", merged,px.size()));

		}
	}

/**
 * 递归获取所有JAR文件的
 * @param inPath 需要搜索的路径
 */
public static void getAllJar(String inPath)
{
	String currentFN = "",n1,n2,n;
	File dir = new File(inPath);
	File[] tfiles = dir.listFiles();
	Properties p;
	Properties nl ;

	if(tfiles==null) return;

	for(int i=0;i<tfiles.length;i++)
	{	currentFN =  tfiles[i].getAbsolutePath();
		if(tfiles[i].isDirectory())
		{
			getAllJar(currentFN);
		}else
		{
			currentFN = currentFN.toLowerCase();
			if(currentFN.endsWith(".jar"))
			{
				if(!jarFiles.contains(currentFN)){
				jarFiles.add(currentFN);
				}
			}
			if(currentFN.endsWith(".properties"))
			{
				files.append(currentFN);
				p=new Properties();
		    	try{
			    	p.load(new FileInputStream(currentFN));
		    	}
		    	catch(Exception e)
		    	{
		    		System.out.println(e.getMessage());
		    	}
//		    	Enumeration<String> eu = (Enumeration<String>) p.propertyNames();
				
				n="en";
			    for(String s: i18n)//遍历可能语种, 设置语种类型
			    {
			    	n1=s+"_";
			    	n2="_"+s;
			    	if(currentFN.contains(n1)||currentFN.contains(n2))//文件名标注语种的
			    	{
			    		n=s;
			    		break;
			    	}
			    }
			    if(!ps.containsKey(n))
			    {	
			    	ps.put(n, new Properties());
			    }
			    nl  = (Properties) ps.get(n);
			    
		    	for(Iterator ks = p.keySet().iterator();ks.hasNext();)
		    	{
		    		n1 = (String)ks.next();
		    		if(nl.contains(n1))
		    		{
		    			nl.remove(n1);
		    		}
		    		nl.put(n1, p.getProperty(n1));
		    	}

			    /*
		    	while(eu.hasMoreElements())//遍历整个properties
		    	{
		    		n1 = (String)eu.nextElement();
		    		if(nl.contains(n1))
		    		{
		    			nl.remove(n1);
		    		}
		    		nl.put(n1, p.getProperty(n1));
		    	}
		    	*/
			}
		}
	}
}
/**
 * 分拣Jar文件中的.properties文件
 * 对于存在 {a}.properties {a}_{b}.properties 的情况, 只加载 {a}.properties , 对{b} 创建新 properties
 * 对于本properties中重复的情况, 替换
 * @param inJar jar路径
 * @throws IOException 
 */
@SuppressWarnings("unchecked")
public static void getProperties(String inJar) throws IOException
{
	ZipFile z = new ZipFile(inJar);
	ZipEntry en = null;
	Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>)z.entries();
	//HashSet<String> fns = new HashSet<String>(); //裸文件名, 如果完全一致的直接合并, 含i18n的合并入对应HashTable里面
	//字符串matcher
	//Pattern regex = Pattern.compile("(?<=filename=\").*?(?=\")"); 
	//Matcher m = regex.matcher("");
	Properties p;
	InputStream ins;
	String tfn="",n1,n2,n;
	Properties nl ;
	while(entries.hasMoreElements())
	{
		
		en = (ZipEntry) entries.nextElement();
		tfn= en.getName().toLowerCase();
		if(tfn.endsWith(".properties"))
		{
			
			files.append(inJar+"/" + tfn);
			System.out.println(">"+inJar+"/" + tfn);
			ins = z.getInputStream(en);
	    	p=new Properties();
	    	try{
		    	p.load(ins);
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println(e.getMessage());
	    	}
	    	Enumeration<String> eu = (Enumeration<String>) p.propertyNames();
    		
			//m=regex.matcher(tfn);
		    //fn=m.group();
	    	n="en";
		    for(String s: i18n)//遍历可能语种, 设置语种类型
		    {
		    	n1=s+"_";
		    	n2="_"+s;
		    	if(tfn.contains(n1)||tfn.contains(n2))//文件名标注语种的
		    	{
		    		n=s;
		    		break;
		    	}
		    }
		    
		    //寻找或创建对应语种存储
		    if(!ps.containsKey(n))
		    {	
		    	ps.put(n, new Properties());
		    }
		    nl  = (Properties) ps.get(n);
		    
	    	while(eu.hasMoreElements())//遍历整个properties
	    	{
	    		n1 = (String)eu.nextElement();
	    		if(nl.contains(n1))
	    		{
	    			nl.remove(n1);
	    		}
	    		nl.put(n1, p.getProperty(n1));
	    	}
		    
		 }
		    
		    
	}
}




}
